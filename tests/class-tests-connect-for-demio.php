<?php
/**
 * Core unit test
 *
 * @package     ConnectForDemio\Tests\Core
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Core unit tests
 *
 * @since       1.0.0
 */
class Tests_Connect_For_Demio extends WP_UnitTestCase {


	/**
	 * Test suite object
	 *
	 * @access      protected
	 * @since       1.0.0
	 * @var         object $object The test suite object
	 */
	protected $object;


	/**
	 * Set up this test suite
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function setUp() {
		parent::setUp();
		$this->object = Connect_For_Demio();
	}


	/**
	 * Tear down this test suite
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function tearDown() { // phpcs:ignore Generic.CodeAnalysis.UselessOverridingMethod
		parent::tearDown();
	}


	/**
	 * Test the Connect_For_Demio instance
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function test_connect_for_demio_instance() {
		$this->assertClassHasStaticAttribute( 'instance', 'Connect_For_Demio' );
	}


	/**
	 * Test constants
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function test_constants() {
		// Make SURE I updated the version.
		$this->assertSame( CONNECT_FOR_DEMIO_VER, '1.0.0' );

		// Plugin folder URL.
		$path = str_replace( 'tests/', '', plugin_dir_url( __FILE__ ) );
		$this->assertSame( CONNECT_FOR_DEMIO_URL, $path );

		// Plugin folder path.
		$path              = str_replace( 'tests/', '', plugin_dir_path( __FILE__ ) );
		$path              = substr( $path, 0, -1 );
		$connect_for_demio = substr( CONNECT_FOR_DEMIO_DIR, 0, -1 );
		$this->assertSame( $connect_for_demio, $path );

		// Plugin root file.
		$path = str_replace( 'tests/', '', plugin_dir_path( __FILE__ ) );
		$this->assertSame( CONNECT_FOR_DEMIO_FILE, $path . 'class-connect-for-demio.php' );
	}


	/**
	 * Test includes
	 *
	 * @access      public
	 * @since       1.0.0
	 * @return      void
	 */
	public function test_includes() {
		$this->assertFileExists( CONNECT_FOR_DEMIO_DIR . 'class-connect-for-demio.php' );
		$this->assertFileExists( CONNECT_FOR_DEMIO_DIR . 'includes/admin/settings/register-settings.php' );
		// $this->assertFileExists( CONNECT_FOR_DEMIO_DIR . 'includes/admin/actions.php' );

		// $this->assertFileExists( CONNECT_FOR_DEMIO_DIR . 'includes/class-connect-for-demio-template-tags.php' );
		// $this->assertFileExists( CONNECT_FOR_DEMIO_DIR . 'includes/misc-functions.php' );
		// $this->assertFileExists( CONNECT_FOR_DEMIO_DIR . 'includes/scripts.php' );

		/** Check Assets Exist */
		// $this->assertFileExists( CONNECT_FOR_DEMIO_DIR . 'assets/css/admin.css' );
		// $this->assertFileExists( CONNECT_FOR_DEMIO_DIR . 'assets/css/admin.min.css' );
		// $this->assertFileExists( CONNECT_FOR_DEMIO_DIR . 'assets/js/admin.js' );
		// $this->assertFileExists( CONNECT_FOR_DEMIO_DIR . 'assets/js/admin.min.js' );
		// $this->assertFileExists( CONNECT_FOR_DEMIO_DIR . 'assets/banner-772x250.jpg' );
		// $this->assertFileExists( CONNECT_FOR_DEMIO_DIR . 'assets/banner-1544x500.jpg' );
		// $this->assertFileExists( CONNECT_FOR_DEMIO_DIR . 'assets/icon-128x128.jpg' );
		// $this->assertFileExists( CONNECT_FOR_DEMIO_DIR . 'assets/icon-256x256.jpg' );
		// $this->assertFileExists( CONNECT_FOR_DEMIO_DIR . 'assets/icon.svg' );
		// $this->assertFileExists( CONNECT_FOR_DEMIO_DIR . 'assets/screenshot-1.png' );
		// $this->assertFileExists( CONNECT_FOR_DEMIO_DIR . 'assets/screenshot-2.gif' );
	}
}
