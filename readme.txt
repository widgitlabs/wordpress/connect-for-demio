=== Connect for Demio ===
Contributors: widgitlabs, evertiro
Donate link: https://evertiro.com/donate/
Tags: demio
Requires at least: 3.0
Tested up to: 5.5.1
Stable tag: 1.0.0

Easy-to-use integration between WordPress and Demio.

== Description ==

Easy-to-use integration between WordPress and Demio.

== Installation ==

= From your WordPress dashboard =

1. Visit 'Plugins > Add New'
2. Search for 'Connect for Demio'
3. Activate Connect for Demio from your Plugins page

= From WordPress.org =

1. Download Connect for Demio
2. Upload the 'connect-for-demio' folder to the '/wp-content/plugins' directory of your WordPress installation
3. Activate Connect for Demio from your Plugins page

== Changelog ==

= Version 1.0.0 =
* Initial release
