# Connect for Demio

## Welcome to our GitLab Repository

Easy-to-use integration between WordPress and Demio.

### Installation

1. You can clone the GitLab repository:
   `https://gitlab.com/widgitlabs/wordpress/connect-for-demio.git`
2. Or download it directly as a ZIP file:
   `https://gitlab.com/widgitlabs/wordpress/connect-for-demio/-/archive/master/connect-for-demio-master.zip`

This will download the latest developer copy of Connect for Demio.

### Bugs

If you find an issue, let us know [here](https://gitlab.com/widgitlabs/wordpress/connect-for-demio/issues?state=open)!

### Contributions

Anyone is welcome to contribute to Connect for Demio. Please read the
[guidelines for contributing](https://gitlab.com/widgitlabs/wordpress/connect-for-demio/-/blob/master/CONTRIBUTING.md)
to this repository.
