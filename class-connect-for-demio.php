<?php
/**
 * Plugin Name:     Connect for Demio
 * Plugin URI:      https://gitlab.com/widgitlabs/wordpress/connect-for-demio
 * Description:     Easy-to-use integration between WordPress and Demio.
 * Author:          Widgit Labs
 * Author URI:      https://widgit.io
 * Version:         1.0.0
 * Text Domain:     connect-for-demio
 * Domain Path:     languages
 *
 * @package         ConnectForDemio
 * @author          Daniel J Griffiths <dgriffiths@evertiro.com>
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


if ( ! class_exists( 'Connect_For_Demio' ) ) {


	/**
	 * Main Connect_For_Demio class
	 *
	 * @access      public
	 * @since       1.0.0
	 */
	final class Connect_For_Demio {


		/**
		 * The one true Connect_For_Demio
		 *
		 * @access      private
		 * @since       1.0.0
		 * @var         Connect_For_Demio $instance The one true Connect_For_Demio
		 */
		private static $instance;


		/**
		 * The settings object
		 *
		 * @access      public
		 * @since       1.0.0
		 * @var         object $settings The settings object
		 */
		public $settings;


		/**
		 * Get active instance
		 *
		 * @access      public
		 * @since       1.0.0
		 * @static
		 * @return      object self::$instance The one true Connect_For_Demio
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Connect_For_Demio ) ) {
				self::$instance = new Connect_For_Demio();
				self::$instance->setup_constants();
				self::$instance->hooks();
				self::$instance->includes();
			}

			return self::$instance;
		}


		/**
		 * Throw error on object clone
		 *
		 * The whole idea of the singleton design pattern is that there is
		 * a single object. Therefore, we don't want the object to be cloned.
		 *
		 * @access      protected
		 * @since       1.0.0
		 * @return      void
		 */
		public function __clone() {
			_doing_it_wrong( __FUNCTION__, esc_attr__( 'Cheatin&#8217; huh?', 'connect-for-demio' ), '1.0.0' );
		}


		/**
		 * Disable unserializing of the class
		 *
		 * @access      protected
		 * @since       1.0.0
		 * @return      void
		 */
		public function __wakeup() {
			_doing_it_wrong( __FUNCTION__, esc_attr__( 'Cheatin&#8217; huh?', 'connect-for-demio' ), '1.0.0' );
		}


		/**
		 * Setup plugin constants
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function setup_constants() {
			// Plugin version.
			if ( ! defined( 'CONNECT_FOR_DEMIO_VER' ) ) {
				define( 'CONNECT_FOR_DEMIO_VER', '1.0.0' );
			}

			// Plugin path.
			if ( ! defined( 'CONNECT_FOR_DEMIO_DIR' ) ) {
				define( 'CONNECT_FOR_DEMIO_DIR', plugin_dir_path( __FILE__ ) );
			}

			// Plugin URL.
			if ( ! defined( 'CONNECT_FOR_DEMIO_URL' ) ) {
				define( 'CONNECT_FOR_DEMIO_URL', plugin_dir_url( __FILE__ ) );
			}

			// Plugin file.
			if ( ! defined( 'CONNECT_FOR_DEMIO_FILE' ) ) {
				define( 'CONNECT_FOR_DEMIO_FILE', __FILE__ );
			}
		}


		/**
		 * Run plugin base hooks
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function hooks() {
			add_action( 'plugins_loaded', array( self::$instance, 'load_textdomain' ) );
		}


		/**
		 * Include necessary files
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function includes() {
			global $connect_for_demio_options;

			// Load settings handler if necessary.
			if ( ! class_exists( 'Simple_Settings' ) ) {
				require_once CONNECT_FOR_DEMIO_DIR . 'vendor/widgitlabs/simple-settings/class-simple-settings.php';
			}

			require_once CONNECT_FOR_DEMIO_DIR . 'vendor/autoload.php';

			require_once CONNECT_FOR_DEMIO_DIR . 'includes/admin/settings/register-settings.php';

			self::$instance->settings  = new Simple_Settings( 'connect-for-demio', 'settings', array( 'sysinfo' ) );
			$connect_for_demio_options = self::$instance->settings->get_settings();

			require_once CONNECT_FOR_DEMIO_DIR . 'includes/misc-functions.php';
			require_once CONNECT_FOR_DEMIO_DIR . 'includes/shortcodes.php';
			require_once CONNECT_FOR_DEMIO_DIR . 'includes/template-tags.php';
		}


		/**
		 * Load plugin language files
		 *
		 * @access      public
		 * @since       1.0.0
		 * @return      void
		 */
		public function load_textdomain() {
			// Set filter for language directory.
			$lang_dir = dirname( plugin_basename( __FILE__ ) ) . '/languages/';
			$lang_dir = apply_filters( 'connect_for_demio_languages_directory', $lang_dir );

			// WordPress plugin locale filter.
			$locale = apply_filters( 'plugin_locale', get_locale(), 'connect-for-demio' );
			$mofile = sprintf( '%1$s-%2$s.mo', 'connect-for-demio', $locale );

			// Setup paths to current locale file.
			$mofile_local  = $lang_dir . $mofile;
			$mofile_global = WP_LANG_DIR . '/connect-for-demio/' . $mofile;
			$mofile_core   = WP_LANG_DIR . '/plugins/connect-for-demio/' . $mofile;

			if ( file_exists( $mofile_global ) ) {
				// Look in global /wp-content/languages/connect-for-demio folder.
				load_textdomain( 'connect-for-demio', $mofile_global );
			} elseif ( file_exists( $mofile_local ) ) {
				// Look in local /wp-content/plugins/connect-for-demio/languages/ folder.
				load_textdomain( 'connect-for-demio', $mofile_local );
			} elseif ( file_exists( $mofile_core ) ) {
				// Look in core /wp-content/languages/plugins/connect-for-demio/ folder.
				load_textdomain( 'connect-for-demio', $mofile_core );
			} else {
				// Load the default language files.
				load_plugin_textdomain( 'connect-for-demio', false, $lang_dir );
			}
		}
	}
}


/**
 * The main function responsible for returning the one true Connect_For_Demio
 * instance to functions everywhere.
 *
 * Use this function like you would a global variable, except without
 * needing to declare the global.
 *
 * Example: <?php $connect_for_demio = Connect_For_Demio(); ?>
 *
 * @since       1.0.0
 * @return      Connect_For_Demio The one true Connect_For_Demio
 */
function connect_for_demio() {
	return Connect_For_Demio::instance();
}

// Get things started.
Connect_For_Demio();
