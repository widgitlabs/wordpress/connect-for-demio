# Connect for Demio

This folder contains translation files for Connect for Demio.

Do not store custom translations in this folder, they will be deleted on updates.
Store custom translations in `wp-content/languages/connect-for-demio`.
