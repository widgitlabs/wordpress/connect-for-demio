<?php
/**
 * Shortcodes
 *
 * @package     ConnectForDemio\Shortcodes
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Events shortcode
 *
 * @since       1.0.0
 * @param       array $atts Attributes passed to the shortcode.
 * @return      string The events list
 */
function connect_for_demio_events_shortcode( $atts ) {
	$template = connect_for_demio()->settings->get_option( 'embed_template', '' );
	return connect_for_demio()->template_tags->do_tags( $template );
}
add_shortcode( 'demio_events', 'connect_for_demio_events_shortcode' );
