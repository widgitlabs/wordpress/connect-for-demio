<?php
/**
 * Register settings
 *
 * @package     ConnectForDemio\Admin\Settings\Register
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Setup the settings menu
 *
 * @since       1.0.0
 * @param       array $menu The default menu settings.
 * @return      array $menu Our defined settings
 */
function connect_for_demio_add_menu( $menu ) {
	$menu['type']       = 'submenu';
	$menu['page_title'] = __( 'Demio Settings', 'connect-for-demio' );
	$menu['menu_title'] = __( 'Demio', 'connect-for-demio' );

	return $menu;
}
add_filter( 'connect_for_demio_menu', 'connect_for_demio_add_menu' );


/**
 * Define our settings tabs
 *
 * @since       1.0.0
 * @param       array $tabs The default tabs.
 * @return      array $tabs Our defined tabs
 */
function connect_for_demio_settings_tabs( $tabs ) {
	$tabs['settings'] = __( 'Settings', 'connect-for-demio' );
	$tabs['support']  = __( 'Support', 'connect-for-demio' );

	return $tabs;
}
add_filter( 'connect_for_demio_settings_tabs', 'connect_for_demio_settings_tabs' );


/**
 * Define settings sections
 *
 * @since       1.0.0
 * @param       array $sections The default sections.
 * @return      array $sections Our defined sections
 */
function connect_for_demio_registered_settings_sections( $sections ) {
	$sections = array(
		'settings' => apply_filters(
			'connect_for_demio_settings_sections_settings',
			array(
				'api'     => __( 'API Settings', 'connect-for-demio' ),
				'display' => __( 'Display Settings', 'connect-for-demio' ),
			)
		),
		'support'  => apply_filters(
			'connect_for_demio_settings_sections_support',
			array(
				'main' => __( 'Connect For Demio Support', 'connect-for-demio' ),
			)
		),
	);

	return $sections;
}
add_filter( 'connect_for_demio_registered_settings_sections', 'connect_for_demio_registered_settings_sections' );


/**
 * Disable save button on unsavable tabs
 *
 * @since       1.0.0
 * @return      array $tabs The updated tabs
 */
function connect_for_demio_define_unsavable_tabs() {
	$tabs = array( 'support' );

	return $tabs;
}
add_filter( 'connect_for_demio_unsavable_tabs', 'connect_for_demio_define_unsavable_tabs' );


/**
 * Define our settings
 *
 * @since       1.0.0
 * @param       array $settings The default settings.
 * @return      array $settings Our defined settings
 */
function connect_for_demio_registered_settings( $settings ) {
	$new_settings = array(
		// General Settings.
		'settings' => apply_filters(
			'connect_for_demio_settings_settings',
			array(
				'api'     => array(
					array(
						'id'   => 'api_settings_header',
						'name' => __( 'API Settings', 'connect-for-demio' ),
						'desc' => '',
						'type' => 'header',
					),
					array(
						'id'   => 'api_key',
						'name' => __( 'API Key', 'connect-for-demio' ),
						'desc' => '',
						'type' => 'text',
					),
					array(
						'id'   => 'api_secret',
						'name' => __( 'API Secret', 'connect-for-demio' ),
						'desc' => '',
						'type' => 'password',
					),
					array(
						'id'   => 'api_status',
						'name' => __( 'Connection Status', 'connect-for-demio' ),
						'desc' => '',
						'type' => 'hook',
					),
				),
				'display' => array(
					array(
						'id'   => 'embed_settings_header',
						'name' => __( 'Embed Settings', 'connect-for-demio' ),
						'desc' => '',
						'type' => 'header',
					),
					array(
						'id'   => 'embed_template',
						'name' => __( 'Embed Template', 'connect-for-demio' ),
						'desc' => Connect_For_Demio()->template_tags->tag_list( array( 'core', 'demio-event' ), __( 'Template Tags', 'connect-for-demio' ) ),
						'type' => 'html',
					),
				),
			)
		),
		'support'  => apply_filters(
			'connect_for_demio_settings_support',
			array(
				'main' => array(
					array(
						'id'   => 'support_header',
						'name' => __( 'Connect for Demio Support', 'connect-for-demio' ),
						'desc' => '',
						'type' => 'header',
					),
					array(
						'id'   => 'system_info',
						'name' => __( 'System Info', 'connect-for-demio' ),
						'desc' => '',
						'type' => 'sysinfo',
					),
				),
			)
		),
	);

	return array_merge( $settings, $new_settings );
}
add_filter( 'connect_for_demio_registered_settings', 'connect_for_demio_registered_settings' );


/**
 * Check the status of the configured API details.
 *
 * @since       1.0.0
 * @return      void
 */
function connect_for_demio_api_status() {
	$api_key    = connect_for_demio()->settings->get_option( 'api_key', false );
	$api_secret = connect_for_demio()->settings->get_option( 'api_secret', false );

	if ( $api_key && $api_secret ) {
		$client = connect_for_demio_api_connect();

		if ( $client ) {
			esc_html_e( 'Connected!', 'connect-for-demio' );

			return;
		} else {
			esc_html_e( 'Invalid API credentials!', 'connect-for-demio' );

			return;
		}
	} else {
		$page_link = '<a href="https://my.demio.com/manage/api-details" target="_blank">' . __( 'Account Settings', 'connect-for-demio' ) . '</a>';

		// translators: 1: The markup for the Account Settings link.
		$status = sprintf( __( 'Disconnected.<br />You can find your API credentials in the Demio %s page.', 'connect-for-demio' ), $page_link );

		echo wp_kses_post( $status );
	}
}
add_action( 'connect_for_demio_api_status', 'connect_for_demio_api_status' );
