<?php
/**
 * Helper functions
 *
 * @package     ConnectForDemio\MiscFunctions
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Connect to the Demio API and return a client object
 *
 * @since       1.0.0
 * @return      mixed object $client if successful, false otherwise
 */
function connect_for_demio_api_connect() {
	$api_key    = connect_for_demio()->settings->get_option( 'api_key', false );
	$api_secret = connect_for_demio()->settings->get_option( 'api_secret', false );

	if ( $api_key && $api_secret ) {
		$client = new \Demio\Client( $api_key, $api_secret );

		$response = $client->ping();

		if ( $response->results()->pong ) {
			return $client;
		}
	}

	return false;
}
