<?php
/**
 * Template tags
 *
 * @package     ConnectForDemio\TemplateTags
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
